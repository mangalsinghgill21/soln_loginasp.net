﻿using Soln_Login.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace Soln_Login
{
    public partial class WebPage1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            if (txtemail!= null && txtpassword!=null)
            {
                personentity personentityobj = new personentity()
                {
                    emailid = txtemail.Text,
                    password= txtpassword.Text                   
                };

                //storing it into json format
                string PersonJsonData = JsonConvert.SerializeObject(personentityobj);

                // Store person Json Value in Session
                Session["personinfo"] = PersonJsonData;

                // Redirect to WebForm 2
                Response.Redirect("~/Webpage2.aspx");
                    
            }
            else
            {
                lblerror.Text="Requirement Invalid";
            }
        }
    }
}