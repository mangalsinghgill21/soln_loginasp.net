﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Soln_Login.Entity;

namespace Soln_Login
{
    public partial class Webpage2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["personinfo"]!=null)
            {
                // read Session Value
                string PersonJsonData = Session["StudentInfo"] as String;

                // DeSerialize Student Json into Student Object
                personentity personentityobj = JsonConvert.DeserializeObject<personentity>(PersonJsonData);

                lblwelcome.Text = "Welcome";
                lblfirstname.Text = personentityobj.firstname;
                lbllastname.Text = personentityobj.lastname;

            }
            else
            {
                Response.Redirect("~/WebPage1.aspx");
            }
        }

        protected void btnlogout_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/WebPage1.aspx");
        }
    }
}