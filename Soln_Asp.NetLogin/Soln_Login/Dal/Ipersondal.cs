﻿using Soln_Login.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_Login.Dal
{
   public  interface Ipersondal
    {
         async Task<bool> login(Ipersonentity personentityobj);
    }
}
