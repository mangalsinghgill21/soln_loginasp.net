﻿using Soln_Login.Entity.Interface;
using Soln_Login.ORD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

namespace Soln_Login.Dal
{
    public class persondal : Ipersondal
    {
        #region Declaration
        private personloginDataContext Dc = null;
        #endregion

        #region Constructor
        public persondal()
        {
            Dc = new personloginDataContext();
        }

       
        #endregion

        public async  Task<bool> login(Ipersonentity personentityobj)
        {
            int? status = null;
            String message = null;

            return await Task.Run(() => {

                var setQuery =
                    Dc
                    .uspsetpersonlogin(
                        "login"
                        ,personentityobj.personid
                        ,personentityobj.emailid
                       , personentityobj.password
                        , personentityobj.firstname
                        ,personentityobj.lastname
                        ,ref status
                        ,ref message
                    );

                return (status == 1) ? true : false;

            });
        }

    }
}