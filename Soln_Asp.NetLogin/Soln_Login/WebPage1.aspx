﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebPage1.aspx.cs" Inherits="Soln_Login.WebPage1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Task Login</title>
    <link href="Css/css.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 800px; text-align: center;">
        <br />
    <table style="width: 140%;">
        <tr>
            <td style="text-align:center"> 
                <asp:TextBox ID="txtemail" runat="server" placeholder="Email" CssClass="txt txt1" />
                <br />
            </td>
        </tr>
        <tr>
            <td> 
                <asp:TextBox ID="txtpassword" runat="server" placeholder="Password" CssClass="txt txt1" />
                <br />
            </td>
        </tr>
        <tr style="text-align:center">
            <td style="align-content:center"> 
                <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn2" Text="Login" OnClick="btnsubmit_Click" />
                <br />
            </td>
        </tr>
        <tr>
            <td> 
               <asp:Label ID="lblerror" runat="server" CssClass="lblerror" />
                <br />
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
