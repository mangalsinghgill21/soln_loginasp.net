﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_Login.Entity.Interface
{
    public interface Ipersonentity
    {
        public dynamic personid { get; set; }
        public dynamic emailid { get; set; }
        public dynamic password { get; set; }
        public dynamic firstname { get; set; }
        public dynamic lastname { get; set; }

    }
}
